import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import Vuex from 'vuex'
import {router} from "./router";
import {createStore} from "./store";

Vue.config.productionTip = false;

Vue.use(Vuex);
Vue.use(VueRouter);

createStore();

export var vue_instance = new Vue({
    render: h => h(App),
    router
});

vue_instance.$mount('#app');
