import LoginForm from "./components/auth/LoginForm";
import Dashboard from "./components/client_area/Dashboard";
import Order from "./components/orders/Order";
import VueRouter from 'vue-router'

const routes = [
    {name: "login", path: "/login", component: LoginForm},
    {name: "dashboard", path: "/", component: Dashboard},
    {name: "order", path: "/order/:id", component: Order},
];

export const router = new VueRouter({routes});

router.beforeEach((to, from, next) => {
    if (!localStorage.session_token && to.path !== "/login") next('/login');
    else next()
});
