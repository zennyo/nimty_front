import {wrapRequestPromise, axios_i} from "./index";

export default {
    getOrders() {
        return wrapRequestPromise(axios_i("client_area/orders/list"));
    }
}
