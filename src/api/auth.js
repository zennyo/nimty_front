import {wrapRequestPromise, axios_i} from "./index";

export default {
    codeLogin(code) {
        return wrapRequestPromise(axios_i.post("client_area/auth/code", { code }));
    },
    telegramLogin(user_object) {
        return wrapRequestPromise(axios_i.post("client_area/auth/telegram", { user_object }));
    },
    getMe() {
        return wrapRequestPromise(axios_i.post("client_area/getMe"));
    },
}
