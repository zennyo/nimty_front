import axios from 'axios'
import {Service} from 'axios-middleware'
import {router} from '../router'

export var axios_i = axios.create({
    baseURL: process.env.VUE_APP_API_BASE,
    timeout: 30000,
});

const service = new Service(axios_i);

service.register({
    onRequest(config) {
        if (localStorage.session_token) {
            config["headers"]["X-Auth-Token"] = localStorage.session_token;
        }
        return config;
    },
    onResponseError(error) {
        let data = error.response["data"] || false;
        if (data) {
            let token_refresh = false;
            try {
                let json = JSON.parse(data);
                token_refresh = json["token_refresh"] || null;
            } catch {
                /* ignored */
            }
            if (token_refresh === true) {
                delete localStorage.session_token;
                router.push("/login")
            }
        }
        throw error;
    }
});

export function wrapRequestPromise (p) {
    return new Promise((resolve, reject) => {
        p.then((response) => {
            let data = response.data;
            if (data.status === "ok") {
                resolve(response.data.data);
            } else if (data.status === "error") {
                reject(response.data.error_message);
            } else {
                reject("Invalid response");
            }
        }).catch((e) => {
            reject("Network error");
            window.console.log(e);
        })
    });
}