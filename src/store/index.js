import orders_api from "../api/orders";
import auth_api from "../api/auth";
import Vuex from 'vuex'

export var store;

export function createStore() {
    store = new Vuex.Store({
        state: {
            account: null,
            hasUpdatedAccount: false,
            orders: [],
            hasUpdatedOrders: false,
        },
        getters: {
            getAccount: (state) => {
                return state.account
            },
            getOrderById: (state) => (id) => {
                return state.orders.find(order => order.id === Number(id));
            }
        },
        mutations: {
            updateAccount(state, account) {
                state.account = account;
                state.hasUpdatedAccount = true;
            },
            updateOrders(state, orders) {
                state.orders = orders;
                state.hasUpdatedOrders = true;
            }
        },
        actions: {
            updateOrders({commit, state}, force) {
                return new Promise((resolve, reject) => {
                    if (force || !state.hasUpdatedOrders) {
                        orders_api.getOrders().then(data => {
                            commit("updateOrders", data.orders);
                            resolve()
                        }).catch(reject)
                    } else {
                        resolve();
                    }
                })
            },
            updateAccount({commit, state}, force) {
                return new Promise((resolve, reject) => {
                    if (force || !state.hasUpdatedAccount) {
                        auth_api.getMe().then(data => {
                            commit("updateAccount", data);
                            resolve()
                        }).catch(reject)
                    } else {
                        resolve();
                    }
                })
            }
        }
    });
}

